var express = require("express");
var mysql = require('mysql');
const bodyParser = require("body-parser");
const bcrypt = require('bcryptjs');
const session = require('express-session');

const app = express();

app.use(session({
    'secret': 'exam'
}))



app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static('public'));
require("dotenv").config();
app.set("view engine", "pug");

var con = mysql.createConnection({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE
});
con.connect(function(err) {
    if (err) {
        console.log(err)
        return
    };
    console.log("Connected!");
});

////////////////connexion & post connexion/////////////// 

app.get("/", function(req, res) {
    res.render("connexion/connect")
})

app.post('/post/connexion', function(req, res) {
    let loginuser = req.body.login
    let mdpuser = req.body.mdp
    if (req.body.login == "admin" && req.body.mdp == "admin") {
        res.redirect("/connexion/admin")
    } else
        con.query(` SELECT * FROM users WHERE login ="${loginuser}";`, (err, result, fields) => {
            if (result.length < 1) {
                res.send("login non existant");
            } else if (loginuser === result[0].login) {
                let mdpverif = bcrypt.compareSync(mdpuser, result[0].password)
                if (mdpverif == true) {
                    req.session.login = result[0].login
                    req.session.iduser = result[0].idusers
                    res.redirect("/accueil");
                } else {
                    res.send("mot de passe invalide")
                }
            } else {
                console.log("page introuvable !!")

                res.send("page introuvable !!")
            }
        });
});




//////////////inscription & post inscription//////////////////

app.get("/inscription", function(req, res) {
    res.render("connexion/inscription")
})

app.post("/post/inscription", function(req, res) {
    let login = req.body.login;
    let mdp = req.body.mdp;
    let mail = req.body.mail;
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(mdp, salt);
    con.query(` SELECT * FROM users WHERE login ="${login}";`, (err, result, fields) => {


        if (result.length < 1) {
            con.query(`INSERT INTO users (login,password,mail) VALUES ("${login}","${hash}" ,"${mail}" );`);

            res.redirect('/');
        } else
            res.send("login existant")
    });


})


//////////////realoadMdp & postRealoadMdp

app.get("/reaload", function(req, res) {
    res.render("connexion/realoadPassword")
})

app.post("/post/realoadMdp", function(req, res) {
    let loginuser = req.body.login
    let mail = req.body.mail
    let mdp = req.body.mdp
    let salt = bcrypt.genSaltSync(10);
    let newMdp = bcrypt.hashSync(mdp, salt);
    con.query(` SELECT * FROM users WHERE login ="${loginuser}";`, (err, result, fields) => {
        console.log(result)
        if (result.length < 1) {
            res.send("login non existent");
        } else if (mail !== result[0].mail) {
            res.send("mail incorrect")
        } else {
            con.query(`UPDATE users SET password= "${newMdp}" WHERE idusers = ${result[0].idusers}`)
            res.redirect("/mdpModif")
        }
    })
})

app.get("/mdpModif", function(req, res) {
    res.render("connexion/mdpModif")
})

////////////////////acceuil///////////////////////

app.get("/accueil", function(req, res) {
        con.query("SELECT * FROM BDDexam.posts;", (err, result, fields) => {

            res.render("post/accueil", { login: req.session.login, results: result })
        })

    })
    /////////////////total posts////////////////////////////////////////////
    /////////////////voir posts/////////////////////

app.get("/voir/post/:idpost", function(req, res) {
    let postVue = req.params.idpost
    con.query(`SELECT * FROM follow;`, (err, result, fields) => {
        let follow = 0;
        for (var i = 0; result.length > i; i++) {
            if (result[i].idPosts == postVue) {
                follow++
            }
        }
        con.query(`SELECT * FROM listComment WHERE idPosts ="${postVue}";`, (err, result, fields) => {
            let comment = result
            con.query(`SELECT * FROM posts WHERE idPosts ="${postVue}";`, (err, result, fields) => {
                // console.log(result)
                // console.log(comment)
                res.render("post/post", { login: req.session.login, post: result, com: comment, follower: follow })
            })
        })
    })
})

///////////////ajout follow/////////////////////

app.post("/follow/ajout", function(req, res) {
    let iduser = req.session.iduser
    let idpost = req.body.idpost

    con.query(`SELECT * FROM follow WHERE idUser ="${iduser}" AND idPosts = "${idpost}";`, (err, result, fields) => {
        if (result.length < 1) {
            con.query(`INSERT INTO follow (idPosts,idUser) VALUES ("${idpost}","${iduser}"  );`);
            res.redirect(`/voir/post/${idpost}`)
        } else
            res.send("Déjà inscrit")


    })
})



/////////////////ajout Comment////////////////////

app.post("/comment/ajout", function(req, res) {

    con.query(`INSERT INTO listComment (listComment,idPosts) VALUES ("${req.body.comment}","${req.body.idPost}");`, (err, result, fields) => {
        if (err) {
            console.log(err)
        };

        res.redirect(`/voir/post/${req.body.idPost}`)

    })

})

//////////////////////////////////////////////////////////////////////////

/////////////////profil//////////////////////////


app.get("/profil", function(req, res) {
    con.query(`SELECT posts.idPosts,title,post,image,date FROM posts INNER JOIN follow ON follow.idUser ="${req.session.iduser}"AND (follow.idPosts = posts.idPosts);`, (err, result, fields) => {
        console.log(result)
        res.render("post/profil", { results: result, login: req.session.login })
    })

})

////////////////////quitter le défi///////////////

app.post("/quitter/defi", function(req, res) {
    con.query(`DELETE  FROM follow WHERE idPosts = ${req.body.idpost} and idUser = ${req.session.iduser};`)
    res.redirect("/profil")
})



/////////////////////////////////////////////admin///////////////////////////////////////////////////////////////////

///////////////////////////////////connexion admin////////////

app.get("/connexion/admin", function(req, res) {
    res.render("adminConnect/connect")
})

app.post('/admin/post/connexion', function(req, res) {
    let loginuser = req.body.login
    let mdpuser = req.body.mdp
    con.query(` SELECT * FROM adminUsers WHERE login ="${loginuser}";`, (err, result, fields) => {
        if (result.length < 1) {
            res.send("login non existant");
        } else if (loginuser === result[0].login) {
            let mdpverif = bcrypt.compareSync(mdpuser, result[0].password)
            if (mdpverif == true) {
                req.session.login = result[0].login
                req.session.iduser = result[0].idusers
                res.redirect("/admin/accueil");
            } else {
                res.send("mot de passe invalide")
            }
        } else {
            console.log("page introuvable !!")

            res.send("page introuvable !!")
        }
    });
});


///////////////////////admin creation mdp/////////////////////

app.get("/admin/inscription", function(req, res) {
    res.render("adminConnect/inscription")
})


app.post("/admin/post/inscription", function(req, res) {
    let login = req.body.login;
    let mdp = req.body.mdp;
    let mail = req.body.mail;
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(mdp, salt);
    con.query(` SELECT * FROM adminUsers WHERE login ="${login}";`, (err, result, fields) => {


        if (result.length < 1) {
            con.query(`INSERT INTO adminUsers (login,password,mail) VALUES ("${login}","${hash}" ,"${mail}" );`);

            res.redirect('/connexion/admin');
        } else
            res.send("login existant")
    });


})

//////////////admin realoadMdp & postRealoadMdp////////////

app.get("/admin/reaload", function(req, res) {
    res.render("adminConnect/realoadPassword")
})

app.post("/admin/post/realoadMdp", function(req, res) {
    let loginuser = req.body.login
    let mail = req.body.mail
    let mdp = req.body.mdp
    let salt = bcrypt.genSaltSync(10);
    let newMdp = bcrypt.hashSync(mdp, salt);
    con.query(` SELECT * FROM adminUsers WHERE login ="${loginuser}";`, (err, result, fields) => {
        console.log(result)
        if (result.length < 1) {
            res.send("login non existent");
        } else if (mail !== result[0].mail) {
            res.send("mail incorrect")
        } else {
            con.query(`UPDATE adminUsers SET password= "${newMdp}" WHERE idadminUsers = ${result[0].idadminUsers}`)
            res.redirect("/admin/mdpModif")
        }
    })
})

app.get("/admin/mdpModif", function(req, res) {
    res.render("adminConnect/mdpModif")
})




///////////////////////admin accueil//////////////////////////

app.get("/admin/accueil", function(req, res) {
    con.query("SELECT * FROM BDDexam.posts;", (err, result, fields) => {
        let posts = result


        res.render("admin/accueil", { post: posts, login: req.session.login })


    })
})

///////////////////////sup posts+ follow et commentaire associer////////////////////

app.post("/admin/sup/post", function(req, res) {

    con.query(`DELETE FROM listComment WHERE idPosts = "${req.body.idpost}"`)
    con.query(`DELETE FROM follow WHERE idPosts = "${req.body.idpost}"`)
    con.query(`DELETE FROM posts WHERE idPosts = "${req.body.idpost}"`)

    res.redirect("/admin/accueil")
})

/////////////////create post/////////////////////

app.get("/create/post", function(req, res) {
    res.render("admin/create", { login: req.session.login })
})

app.post("/admin/post/article", function(req, res) {
    let title = req.body.title
    let post = req.body.post
    let img = req.body.img
    con.query(`INSERT 
    INTO posts (title,post,image,date) VALUES ('${title}','${post}','${img}',now());`, (err, result, fields) => {
        if (err) {
            console.log(err)
        }
    });
    res.redirect("/admin/accueil")
})

///////////////////modif post titre////////////////////

app.post("/modif/titre/post", function(req, res) {
    con.query(`UPDATE posts SET title = "${req.body.title}" WHERE idPosts = "${req.body.idpost}"`)
    res.redirect("/admin/accueil")
})

//////////////////modif post com///////////////////////////

app.post("/modif/description/post", function(req, res) {
    con.query(`UPDATE posts SET post = "${req.body.post}" WHERE idPosts = "${req.body.idpost}"`)
    res.redirect("/admin/accueil")
})

//////////////////modif post img///////////////////////////

app.post("/modif/img/post", function(req, res) {
    con.query(`UPDATE posts SET image = "${req.body.img}" WHERE idPosts = "${req.body.idpost}"`)
    res.redirect("/admin/accueil")
})

///////////////////afficher & sup utilisateur///////////////

app.get("/affichSup/utilisateur", function(req, res) {
    con.query("SELECT * FROM users", (err, result, fields) => {
        res.render("admin/utilisateur", { results: result, login: req.session.login })

    })
})

/////////////////sup utilisateur////////////////////////////
app.post("/ban/utilisateur", function(req, res) {
    con.query(`DELETE FROM follow WHERE idUser = ${req.body.iduser}`)
    con.query(`DELETE FROM users WHERE idusers = ${req.body.iduser}`)
    res.redirect("/affichSup/utilisateur")
})















app.listen(process.env.PORT, () => console.log(`lancé sur port ${process.env.PORT}`));